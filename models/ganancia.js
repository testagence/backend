var db = require('../config/db');

exports.getInformeGanancias = getInformeGanancias;

function getInformeGanancias(user, fromYear, fromMonth, toYear, toMonth) {

    let promise = new Promise(promiseFunc);
    return promise;

    function promiseFunc(resolve, reject) {
        getCostoFijo(user)
            .then(getCostoFijoDone)
            .catch(handleError);

        function getCostoFijoDone(objetoSalario) {
            let salarioBruto = objetoSalario.brut_salario;
            getGananciaComision(user, salarioBruto, fromYear, fromMonth, toYear, toMonth)
                .then(getGananciaComisionDone)
                .catch(handleError)

            function getGananciaComisionDone(results) {
                let resultsUser={
                    user: user,
                    brut_salario:salarioBruto,
                    resultados_by_periodos: results
                }
                return resolve(resultsUser);
            }

        }

        function handleError(err) {
            return reject(err);
        }
    }


}

function getGananciaComision(user, salarioBruto, fromYear, fromMonth, toYear, toMonth) {

    let numFromYear = parseInt(fromYear);
    let numFromMonth = parseInt(fromMonth);
    let numToYear = parseInt(toYear);
    let numToMonth = parseInt(toMonth);

    let consultaSql = "SELECT EXTRACT( YEAR_MONTH FROM 'data_emissao' ) as year_m, " +
        "SUM( a.valor - (a.valor * (a.total_imp_inc /100))) as valor_neto_periodo, " +
        "SUM( (a.valor - (a.valor * (a.total_imp_inc /100))) * (a.comissao_cn /100)  ) as comision_periodo, " +
        "YEAR(data_emissao) as anyo, " +
        "MONTH(data_emissao) as mes " +
        "FROM cao_fatura a " +
        "INNER JOIN cao_os b ON a.co_os=b.co_os " +
        "INNER JOIN cao_cliente c ON a.co_cliente=c.co_cliente " +
        "INNER JOIN cao_usuario d ON b.co_usuario=d.co_usuario " +
        "WHERE d.co_usuario='" + user + "' " +
        "AND (YEAR(data_emissao) >=" + numFromYear + " AND MONTH (data_emissao)>=" + numFromMonth + ") " +
        "AND (YEAR(data_emissao) <=" + numToYear + " AND MONTH (data_emissao)<=" + numToMonth + ") " +
        "group by year_m,anyo,mes ";

    let promise = new Promise(promiseFunc);
    return promise;

    function promiseFunc(resolve, reject) {
        db.get().query(consultaSql, getGananciaComisionDone);

        function getGananciaComisionDone(err, rows, fields) {
            if (err) return reject(err);
            let arregloFinal = [];

            for (let i = 0; i < rows.length; i++) {
                let item = rows[i];
                item.brut_salario = salarioBruto;
                item.mes= fixMonth(item.mes);
                item.year_m= item.anyo + "-" + item.mes;
                item.lucro = item.valor_neto_periodo - item.brut_salario - item.comision_periodo;
                arregloFinal.push(item);
            }


            return resolve(arregloFinal);
        }
    }


}


function getCostoFijo(user) {
    let consultaSql = "SELECT brut_salario FROM cao_salario WHERE co_usuario='" + user + "' ORDER BY dt_alteracao " +
        " DESC LIMIT 1";

    let promise = new Promise(promiseFunc);
    return promise;

    function promiseFunc(resolve, reject) {
        db.get().query(consultaSql, getCostosDone);

        function getCostosDone(err, rows) {
            if (err) return reject(err);

            if (rows.length > 0) {
                return resolve(rows[0]);
            } else {
                return resolve({ "brut_salario": 0 });
            }

        }
    }


}

function fixMonth(value) {
    if (value < 10) {
        return "0" + value;
    } else {
        return value.toString();
    }
}