var db = require('../config/db');


exports.getConsultores = getConsultores;

function getConsultores() {

    let consultaSQL = "SELECT a.co_usuario,a.no_usuario FROM  cao_usuario a " +
        "INNER JOIN permissao_sistema b ON a.co_usuario = b.co_usuario " +
        "WHERE b.co_sistema=1 AND b.in_ativo='S' AND b.co_tipo_usuario IN (0,1,2) ORDER BY a.no_usuario";
    let promise = new Promise(promiseFunc);

    return promise;

    function promiseFunc(resolve, reject) {
        db.get().query(consultaSQL, getConsultoresDone);

        function getConsultoresDone(err, rows) {
            if (err) return reject(err);
            
            return resolve(rows);


        }
    }

}

