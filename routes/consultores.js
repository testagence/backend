var express = require('express');
var router = express.Router();

var consultoresCtrl = require('../controllers/consultores.ctrl');

router.get('/', consultoresCtrl.getConsultores);

module.exports = router;
