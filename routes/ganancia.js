var express = require('express');
var router = express.Router();

var gananciaCtrl = require('../controllers/ganancia.ctrl');

router.get('/:user/:fromYear/:fromMonth/:toYear/:toMonth', gananciaCtrl.getInformeGanancias);

module.exports = router;
