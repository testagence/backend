var mysql = require('mysql');

var PRODUCTION_DB = 'test_agence'
  , TEST_DB = 'test_agence'

exports.MODE_TEST = 'test_agence'
exports.MODE_PRODUCTION = 'test_agence'

var state = {
  pool: null,
  mode: null,
}

exports.connect = function(mode, done) {
  state.pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'secret',
    database: mode === exports.MODE_PRODUCTION ? PRODUCTION_DB : TEST_DB
  })

  state.mode = mode
  done()
}

exports.get = function() {
  return state.pool
}


