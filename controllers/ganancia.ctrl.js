var gananciaModel = require('../models/ganancia');

exports.getInformeGanancias = getInformeGanancias;

function getInformeGanancias(req, res, next) {
    let user = req.params.user;
    let fromYear = req.params.fromYear;
    let fromMonth = req.params.fromMonth;
    let toYear = req.params.toYear;
    let toMonth = req.params.toMonth;
    gananciaModel.getInformeGanancias(user, fromYear, fromMonth, toYear, toMonth)
        .then(getInformeGananciasDone)
        .catch(getInformeGananciasError);

    function getInformeGananciasDone(results) {
        return res.status(200).json(results);
    }
    function getInformeGananciasError(err) {
        return next(err);
    }
}
