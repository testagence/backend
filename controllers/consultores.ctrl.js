var consultoresModel= require('../models/consultores');

exports.getConsultores = getConsultores;

function getConsultores (req,res,next){

    consultoresModel.getConsultores()
        .then(getConsultoresDone)
        .catch(getConsultoresErr);

        function getConsultoresDone(results){
            let resultObject = {
                status:true,
                result: results
            };
            return res.status(200).json (resultObject);
        }

        function getConsultoresErr(err){
            return next(err);
        }



}
